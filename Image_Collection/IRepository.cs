﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Image_Collection
{
    public interface IRepository
    {
        Task Add (Image image);
        Task Get();
        Task DownLoad(int id);
        string ShowListImage();
        void Dispose();
        Task IsDownload(int id);
    }
}
