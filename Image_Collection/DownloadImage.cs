﻿using Newtonsoft;
using Newtonsoft.Json;
using System.Net.Http.Json;
using System.Text;

namespace Image_Collection
{
    public class DownloadImage : IRepository
    {
        const string url = "https://64c0b9550d8e251fd11271fe.mockapi.io/api/todoApp/";
        const string directoryPath = @"Directory";
        const string endPoint = "Image";
        List<Image>? images;
        HttpClient client;
        public DownloadImage()
        {
            images = new List<Image>();
            client = new HttpClient();
        }
        public async Task Add(Image image)
        {
            string urlPath = string.Concat(url, endPoint);
            await client.PostAsJsonAsync(urlPath, image);
        }
        public async Task Get()
        {
            string urlPath = string.Concat(url, endPoint);
            var access = await client.GetAsync(urlPath);
            var data = await access.Content.ReadAsStringAsync();
            images = JsonConvert.DeserializeObject<List<Image>>(data);  
        }
        public void Dispose()
        {
            client.Dispose();
        }
        public async Task DownLoad(int id)
        {
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            foreach(var image in images!)
            {
                if (image.Id == id && image.IsDownload is false)
                {
                    string fileName = Path.GetFileName(image.Url);
                    var data = await client.GetByteArrayAsync(image.Url);
                    string location = string.Concat(directoryPath, "/" + fileName);
                    await File.WriteAllBytesAsync(location, data);
                }
                if(image.IsDownload is true)
                {
                    throw new Exception("Image has already download");
                }
            }
        }
        

        public string ShowListImage()
        {
            StringBuilder sb = new StringBuilder();
            foreach(var item in images!)
            {
                sb.AppendLine($"{item.Id} | {item.Description}");
            }
            return sb.ToString();
        }

        public async Task IsDownload(int id)
        {
            string urlPath = string.Concat(url, endPoint + $"/{id}");
            var image = images.Find(img => img.Id == id);
            if(image == null)
            {
                throw new Exception("Can not found by " + id);
            }
            image.IsDownload = true;
            await client.PutAsJsonAsync(urlPath, image);
        }
    }
}
