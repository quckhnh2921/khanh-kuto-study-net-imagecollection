﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Image_Collection
{
    public class Image
    {
        private string? url;
        private int id;
        private string? description;
        private bool isDownload = false;
        public bool IsDownload
        {
            get => isDownload;
            set => isDownload = value;
        }
        public string Description
        {
            get => description!; 
            set => description = value;
        }
        public int Id
        {
            get => id;
            set => id = value;  
        }
        public string Url
        {
            get => url!;
            set => url = value;
        }
    }
}
